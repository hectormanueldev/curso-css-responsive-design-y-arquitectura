# Curso de CSS Responsive Design y Arquitectura

## Temario

1. :heavy_plus_sign: Contenedores flexibles
1. :heavy_plus_sign: Propiedades max-width y min-width, max-height y min-height
1. :heavy_plus_sign: Tamaños fijos vs tamaños máximos y mínimos
1. :heavy_plus_sign: Multimedia flexible
1. :heavy_plus_sign: Atributo srcset y sizes
1. :heavy_plus_sign: Etiqueta picture
1. :heavy_plus_sign: Videos responsivos
1. :heavy_plus_sign: iFrames responsivos
1. :heavy_plus_sign: Media queries versión 3
1. :heavy_plus_sign: Grid responsiva artesanal con flexbox
1. :heavy_plus_sign: Feature queries
1. :heavy_plus_sign: Container queries
1. :heavy_plus_sign: Grid fluida
1. :heavy_plus_sign: Textos fluidos
1. :heavy_plus_sign: Contenedores fluidos
